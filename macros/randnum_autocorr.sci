// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = randnum_autocorr(y,lags)
    // Autocorrelation of y
    // 
    // Calling Sequence
    //   r = randnum_autocorr(y,lags)
    //
    // Parameters
    // y : a N-by-1 matrix of doubles, the data
    // lags : a K-by-1 matrix of doubles, integer value, the lags
    // r : a K-by-1 matrix of doubles, the autocorrelation of y with lags
    //
    // Description
    //   Computes the autocorrelation of y with lags.
    //
    // Examples
    // // Autocorrelation with lags 0, 1,..., 5
    // // r = [1.   0.27   0.16  -0.10   0.06   0.07]'
    // // Reference:
    // // Random number generation and Monte-Carlo methods
    // // James Gentle, Second Edition
    // // Springer, 2005
    // // Section 1.2.1, Structure in the generated numbers
    // y = [
    //     27.    29.    30.    15.    23.  
    //     19.    25.    28.    14.    7.   
    //     26.    13.    22.    11.    21.  
    //     16.    8.     4.     2.     1.   
    //     17.    24.    12.    6.     3.   
    //     20.    10.    5.     18.    9.   
    // ];
    // y = y/31;
    // r = randnum_autocorr(y,[0 1 2 3 4 5])
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    //   NIST/SEMATECH e-Handbook of Statistical Methods, http://www.itl.nist.gov/div898/handbook/eda/section3/eda35c.htm

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_autocorr" , rhs , 2 )
    apifun_checklhs ( "randnum_autocorr" , lhs , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_autocorr" , y , "y" , 1 , "constant" )
    apifun_checktype ( "randnum_autocorr" , lags , "lags" , 2 , "constant" )
    //
    // Check size
    apifun_checkvector ( "randnum_autocorr" , y , "y" , 1 )
    apifun_checkvector ( "randnum_autocorr" , lags , "lags" , 2 )
    //
    // Check content
    apifun_checkflint ( "randnum_autocorr" , lags , "lags" , 2 )
    apifun_checkrange ( "randnum_autocorr" , lags , "lags" , 2 , 0 , 2^32 )
    //
    // Proceed...

    y = y(:);
    N = size(y,"*")	
    ymean = mean(y)
    r = []
    lags = lags(:)'
    for k = lags
        p = (y(1:N-k)-ymean).*(y(k+1:N)-ymean)
        S = sum(p)
        v = sum((y-ymean).^2)
        r($+1) = S/v
    end
endfunction
