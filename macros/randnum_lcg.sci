// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [value,seed] = randnum_lcg(varargin)
    // Random numbers from a linear congruential generator
    // 
    // Calling Sequence
    //   [value,seed] = randnum_lcg(a,b,m,seed)
    //   [value,seed] = randnum_lcg(a,b,m,seed,n)
    //   [value,seed] = randnum_lcg(a,b,m,seed,n,d)
    //
    // Parameters
    // a : a 1-by-1 matrix of doubles, integer value, positive, the multiplier of the LCG
    // b : a 1-by-1 matrix of doubles, integer value, positive, the shift of the LCG
    // m : a 1-by-1 matrix of doubles, integer value, positive, the modulo of the LCG
    // seed : a 1-by-1 matrix of doubles, integer value, a seed for the random number generator. The seed must be in the set 0,1,2,...,m-1. 
    //    n : 1-by-1 matrix of doubles, integer value, the number of samples (default n=1)
    //    d : 1-by-1 matrix of doubles, integer value, the dimension (default d=1)
    // value : n-by-d matrix of doubles, a pseudorandom value uniform in (0,1).
    //
    // Description
    // Computes values from a linear congruential generator defined by the 
    // recurrence :
    //
    // <screen>
    // s = a*s+b (mod m)
    // </screen>
    //
    // The examples below are based on the book by J.E. Gentle, 
    // "Random Number Generation and Monte Carlo Methods", 
    // and more precisely, Chapter 1. "Simulating Random Numbers 
    // from a Uniform Distribution".
    //
    // Examples
    // // Numbers from x=3*x (mod 31)
    // // 3 is a primitive root modulo 31.
    // a = 3
    // b = 0
    // m = 31
    // seed = 1
    // [value,seed] = randnum_lcg(a,b,m,seed)
    // [value,seed] = randnum_lcg(a,b,m,seed)
    // // Check that 3 is a primitive root modulo 31 :
    // // number_primitiveroot(31,%inf)
    // 
    // // Compute all the values from this LCG.
    // a = 3
    // b = 0
    // m = 31
    // seed = 9;
    // [U,seed] = randnum_lcg(a,b,m,seed,m-1)
	// // Compute the autocorrelation coefficient.
    // randnum_autocorr(U,[0 1 2 3 4 5])
    // scf();
    // plot((0:5),r,"bo-");
    // xtitle("Autocorrelation of MCG with m=31 and a=3",..
    // "Lag","Autocorrelation");
    // 
	// // Reference: 
	// // Random Number Generation and Monte Carlo Methods, 
	// // by Jame Gentle
    // // Figure 1.3: "Pairs of successive overlapping 
	// // numbers from x=3*x (mod 31)
    // h = scf();
    // plot([U(1:30)],[U(2:30);U(1)],"bo")
    // xtitle("Overlapping pairs from x=3*x (mod 31)",..
	// "x(i)","x(i+1)")
    //
	// // Plot of non-overlapping pairs
	// a = 3;
	// b = 0;
	// m = 31;
	// seed = 9;
	// [value,seed] = randnum_lcg(a,b,m,seed,m-1,2);
	// plot(value(:,1),value(:,2),".");
	// xtitle("Non overlapping pairs from x=3*x (mod 31)",..
	// "X1","X2")
	//
	// // Plot overlapping triplets in 3D
	// a = 11;
	// m = 31;
	// d = 1;
	// seed = 1;
	// b = 0;
	// [value,seed] = randnum_lcg(a,b,m,seed,m-1);
	// V1 = value(1:m-3);
	// V2 = value(2:m-2);
	// V3 = value(3:m-1);
	// h = scf();
	// param3d(V1,V2,V3);
	// h.children.children.mark_mode="on";
	// h.children.children.line_mode="off";
	// h.children.children.mark_size=1;
	//
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    // Random Number Generation and Monte Carlo Methods, Second Edition, by James E. Gentle, Springer-Verlag, 2003.

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_lcg" , rhs , 4:6 )
    apifun_checklhs ( "randnum_lcg" , lhs , 2 )
    //
    a = varargin(1)
    b = varargin(2)
    m = varargin(3)
    seed = varargin(4)
    n = apifun_argindefault ( varargin , 5 , 1 )
    d = apifun_argindefault ( varargin , 6 , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_lcg" , a , "a" , 1 , "constant" )
    apifun_checktype ( "randnum_lcg" , b , "b" , 2 , "constant" )
    apifun_checktype ( "randnum_lcg" , m , "m" , 3 , "constant" )
    apifun_checktype ( "randnum_lcg" , seed , "seed" , 4 , "constant" )
    apifun_checktype ( "randnum_lcg" , n , "n" , 5 , "constant" )
    apifun_checktype ( "randnum_lcg" , d , "d" , 6 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_lcg" , a , "a" , 1 )
    apifun_checkscalar ( "randnum_lcg" , b , "b" , 2 )
    apifun_checkscalar ( "randnum_lcg" , m , "m" , 3 )
    apifun_checkscalar ( "randnum_lcg" , seed , "seed" , 4 )
    apifun_checkscalar ( "randnum_lcg" , n , "n" , 5 )
    apifun_checkscalar ( "randnum_lcg" , d , "d" , 6 )
    //
    // Check content
    apifun_checkflint ( "randnum_lcg" , a , "a" , 1 )
    apifun_checkgreq ( "randnum_lcg" , a , "a" , 1 , 1 )
    apifun_checkflint ( "randnum_lcg" , b , "b" , 2 )
    apifun_checkgreq ( "randnum_lcg" , b , "b" , 2 , 0 )
    apifun_checkflint ( "randnum_lcg" , m , "m" , 3 )
    apifun_checkgreq ( "randnum_lcg" , m , "m" , 3 , 1 )
    apifun_checkflint ( "randnum_lcg" , seed , "seed" , 4 )
    apifun_checkrange ( "randnum_lcg" , seed , "seed" , 4 , 0 , m-1 )
    apifun_checkflint ( "randnum_lcg" , n , "n" , 5 )
    apifun_checkgreq ( "randnum_lcg" , n , "n" , 5 , 1 )
    apifun_checkflint ( "randnum_lcg" , d , "d" , 6 )
    apifun_checkgreq ( "randnum_lcg" , d , "d" , 6 , 1 )
    //
    // Proceed...
	value = zeros(n,d)
    for i = 1 : n
        for j = 1 : d
		// TODO : Use Shrage method
            seed = pmodulo(a*seed+b,m)
            value(i,j) = seed/m
        end
    end
endfunction

