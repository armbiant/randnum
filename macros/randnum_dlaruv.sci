// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ value , this ] = randnum_dlaruv (varargin)
    // Generate uniform random numbers as in LAPACK/DLARUV.
    // 
    // Calling Sequence
    //   [value,rng] = randnum_dlaruv (rng)
    //   [value,rng] = randnum_dlaruv (rng,n)
    //
    // Parameters
    // rng : a data structure.
    // n : 1-by-1 matrix of doubles, integer value, the number of samples (default n=1)
    // value : a n-by-d matrix of doubles, pseudorandom numbers uniform in (0,1)
    //
    // Description
    //   Generate uniform random numbers from the LAPACK/DLARUV random number generator.
    //
    // Examples
    // // In dimension 1
    // s = [1 1 1 1];
    // d = 1;
    // rng = randnum_dlaruvstart(d,s);
    // [value,rng] = randnum_dlaruv (rng,10)
    //
    // // In dimension 4
    // s = [1 1 1 1];
    // d = 4;
    // rng = randnum_dlaruvstart(d,s);
    // [value,rng] = randnum_dlaruv (rng,10)
	//
	// // Plot 500 samples in dimension 2
    // s = [1 1 1 1];
    // d = 2;
    // rng = randnum_dlaruvstart(d,s);
    // [value,rng] = randnum_dlaruv (rng,500);
	// plot(value(:,1),value(:,2),".");
	// xtitle("DLARUV Random Numbers","X1","X2")
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    //   G.S. Fishman, 'Multiplicative congruential random number generators with modulus  2**b: an exhaustive analysis for b = 32 and a partial analysis for b = 48', Math. Comp. 189, pp 331-344, 1990
    // http://www.netlib.org/lapack/explore-html/d5/d46/dlaruv_8f_source.html

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_dlaruv" , rhs , 1:2 )
    apifun_checklhs ( "randnum_dlaruv" , lhs , 2 )
    //
    this = varargin(1)
    n = apifun_argindefault ( varargin , 2 , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_dlaruv" , this , "this" , 1 , "T_DLARUV" )
    apifun_checktype ( "randnum_dlaruv" , n , "n" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_dlaruv" , n , "n" , 2 )
    //
    //
    // Check content
    apifun_checkflint ( "randnum_dlaruv" , n , "n" , 2 )
    apifun_checkgreq ( "randnum_dlaruv" , n , "n" , 2 , 1 )
    // Proceed...

    I1 = this.dlaruv_previous( 1 )
    I2 = this.dlaruv_previous( 2 )
    I3 = this.dlaruv_previous( 3 )
    I4 = this.dlaruv_previous( 4 )
    MM = this.dlaruvMM
    R = this.dlaruvR
    IPW2 = this.dlaruvIPW2
    value = zeros(n,this.dlaruv_d)
    //
    for i = 1 : n
        for j = 1 : this.dlaruv_d
            for iloop = 1:2
                //
                // Multiply the seed by i-th power of the multiplier modulo 2**48
                IT4 = I4*MM( j, 4 )
                IT3 = floor(IT4 / IPW2)
                IT4 = IT4 - IPW2*IT3
                IT3 = IT3 + I3*MM( j, 4 ) + I4*MM( j, 3 )
                IT2 = floor(IT3 / IPW2)
                IT3 = IT3 - IPW2*IT2
                IT2 = IT2 + I2*MM( j, 4 ) + I3*MM( j, 3 ) + I4*MM( j, 2 )
                IT1 = floor(IT2 / IPW2)
                IT2 = IT2 - IPW2*IT1
                IT1 = IT1 + I1*MM( j, 4 ) + I2*MM( j, 3 ) + I3*MM( j, 2 ) + I4*MM( j, 1 )
                IT1 = modulo ( IT1, IPW2 )
                //
                // Convert 48-bit integer to a real number in the interval (0,1)
                //
                value(i,j) = R*( IT1+R*( IT2+R*( IT3 +R* IT4 ) ) )
                if (value(i,j) <> 1.0) then
                    break
                end
                // If a real number has n bits of precision, and the first
                // n bits of the 48-bit integer above happen to be all 1 (which
                // will occur about once every 2**n calls), then value( j ) will
                // be rounded to exactly 1.0.
                // Since value( j ) is not supposed to return exactly 0.0 or 1.0,
                // the statistically correct thing to do in this situation is
                // simply to iterate again.
                // N.B. the case value( j ) = 0.0 should not be possible.
                I1 = I1 + 2
                I2 = I2 + 2
                I3 = I3 + 2
                I4 = I4 + 2
            end
        end
        I1 = IT1
        I2 = IT2
        I3 = IT3
        I4 = IT4
    end
    //
    // Return final value of seed
    this.dlaruv_previous( 1 ) = IT1
    this.dlaruv_previous( 2 ) = IT2
    this.dlaruv_previous( 3 ) = IT3
    this.dlaruv_previous( 4 ) = IT4
endfunction
