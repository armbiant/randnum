// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function values = randnum_combinesumk(k,s,d)
    // Returns all d-uplets which sum to s.
    //
    // Calling Sequence
	// values = randnum_combinesumk(k,s,d)
    // 
    // Parameters
    // k : a 1-by-1 matrix of doubles, integer value, the maximum value in the uplets
    // s : a 1-by-1 matrix of doubles, integer value, the sum
    // d : a 1-by-1 matrix of doubles, integer value, the dimension
    // values : a m-by-d matrix of doubles, integer value, the combinations
    //
    // Description
    // Returns all uplets (u1,u2,...,ud) 
    // where u1,u2,...,ud in {0,1,2,...,k} 
    // such that u1+u2+...+ud==s.
	//
	// The k-th uplet is 
    //<screen>
    // u1 = values(k,1)
    // u2 = values(k,2)
    // ...
    // ud = values(k,d)
    //</screen>
	//
	// for k=1,2,...,m.
	//
    // Examples
    // // All 2-uplets in {0,1,2,3,4} which sum to s=3.
    // values = randnum_combinesumk(4,3,2)
	//
	// k = 2;
	// d = 3;
	// for s = 0 : k
	//     values = randnum_combinesumk(k,s,d);
	//     mprintf("#{%d-uplets which sum to %d}=%d\n",..
	//     d,s,size(values,"r"))
	// end
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_combinesumk" , rhs , 3 )
    apifun_checklhs ( "randnum_combinesumk" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "randnum_combinesumk" , k , "k" , 1 , "constant" )
    apifun_checktype ( "randnum_combinesumk" , s , "s" , 2 , "constant" )
    apifun_checktype ( "randnum_combinesumk" , d , "d" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_combinesumk" , k , "k" , 1 )
    apifun_checkscalar ( "randnum_combinesumk" , s , "s" , 2 )
    apifun_checkscalar ( "randnum_combinesumk" , d , "d" , 3 )
    //
    // Check content
    apifun_checkflint ( "randnum_combinesumk" , k , "k" , 1 )
    apifun_checkgreq  ( "randnum_combinesumk" , k , "k" , 1 , 0 )
    apifun_checkflint ( "randnum_combinesumk" , s , "s" , 2 )
    apifun_checkgreq  ( "randnum_combinesumk" , s , "s" , 2 , 0 )
    apifun_checkflint ( "randnum_combinesumk" , d , "d" , 3 )
    apifun_checkgreq  ( "randnum_combinesumk" , d , "d" , 3 , 1 )
	//
    if (k>s) then
        // We cannot achieve the sum: 
        // a smaller set {0,1,2,...,s}
        // suffices.
        k=s
    end

    if (d==2) then
        kmin = max(0,s-k)
        v = (kmin:k)'
        values = [v,s-v]
    else
        values = []
        n = 0
        for i = 0:k
            V = randnum_combinesumk(k,s-i,d-1)
            m = size(V,"r")
            vi = ones(m,1)*i
            values(n+1:n+m,1:d) = [vi,V]
            n = n+m
        end
    end
endfunction
