// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function randnum_plotlines(c1,c2)
    // Plot lines with given coefficients
    // 
    // Calling Sequence
    //   randnum_plotlines(c1,c2)
    //
    // Parameters
    // c1 : a 1-by-1 matrix of doubles, the first coefficient
    // c2 : a 1-by-1 matrix of doubles, integer value, positive, the modulo of the LCG
    //
    // Description
    // Plot lines of [0,1]^2 such that 
    //
    // <screen>
    // c1*x + c2*y = k,
    // </screen>
    //
    // for k=-|c1|-|c2|,-|c1|-|c2|+1,...,|c1|+|c2|.
    //
    // Examples
	// // Plots the lines for a=3, m=31 
	// // and the couple (c1=3,c2=-1)
	// scf();
	// a = 3;
	// b = 0;
	// m = 31;
	// randnum_lcgplot(a,b,m)
	// c1 = 3;
	// c2 = -1;
	// randnum_plotlines(c1,c2)
	//
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    // Random numbers fall mainly in the planes, G. Marsaglia, Proc National Academy of Sciences, 61, 25-28, 1968

  [lhs, rhs] = argn()
  apifun_checkrhs ( "randnum_plotlines" , rhs , 2 )
  apifun_checklhs ( "randnum_plotlines" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "randnum_plotlines" , c1 , "c1" , 1 , "constant" )
  apifun_checktype ( "randnum_plotlines" , c2 , "c2" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "randnum_plotlines" , c1 , "c1" , 1 )
  apifun_checkscalar ( "randnum_plotlines" , c2 , "c2" , 2 )
  //
  // Check content
  apifun_checkflint ( "randnum_plotlines" , c1 , "c1" , 1 )
  apifun_checkflint ( "randnum_plotlines" , c2 , "c2" , 2 )
  //
    N = 100;
    nblines = abs(c1)+abs(c2);
	drawlater()
    for k = -nblines:nblines
        x = linspace(0,1,N);
        y = k/c2 - c1/c2*x;
        plot(x,y,"r-");
    end
    h = gcf();
    h.children.data_bounds = [0,0;1,1];
    thetitle=msprintf("Number of lines : %d",nblines);
    xtitle(thetitle,"x(i)","x(i+1)");
    drawnow()
endfunction
