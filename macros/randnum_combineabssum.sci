// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [values,indices] = randnum_combineabssum(s,varargin)
    // Returns the combinations which absolute value sums to s.
    // 
    // Calling Sequence
    //  values = randnum_combineabssum(s,a1,a2)
    //  values = randnum_combineabssum(s,a1,a2,a3)
    //  values = randnum_combineabssum(s,a1,a2,a3,a4,...)
    //  [values,indices] = randnum_combineabssum(s,a1,...)
    //  
    // Parameters
    // s : a 1-by-1 matrix of doubles, integer value, the sum
    // a1 : a n1-by-1 matrix of doubles, integer value
    // a2 : a n2-by-1 matrix of doubles, integer value
    // a3 : a n3-by-1 matrix of doubles, integer value
    // a4 : a n4-by-1 matrix of doubles, integer value
    // values : a m-by-n matrix of doubles, where n is the number of arguments to combine and m is the number of matching combinations. On output, we have sum(abs(values),"c")==s.
    // indices : a m-by-n matrix of doubles, where n is the number of arguments to combine  and m is the number of matching combinations. The indices corresponding to the arguments.
    //
    // Description
    // Returns all uplets (i1,i2,i3,...) such that 
    //<screen>
    // |a1(i1)|+|a2(i2)|+|a3(i3)|+...=s.
    //</screen>
	//
    // The k-th uplet is :
    //<screen>
    // i1 = indices(k,1)
    // i2 = indices(k,2)
    // i3 = indices(k,3)
    // ...
    //</screen>
	// and the corresponding values are 
    //<screen>
    // a1(i1) = values(k,1)
    // a1(i2) = values(k,2)
    // a1(i3) = values(k,3)
    // ...
    //</screen>
	// for k= 1, 2, ..., m.
	//
	// Up to 1000 arguments can be combined.
	//
    // Examples
	// // Find all couples (a(i),b(j)) such that |a(i)|+|b(j)| = 2
	// a = -3:3
	// b = -2:2
	// [values,indices] = randnum_combineabssum(2,a,b)
	// S = sum(abs(values),"c")
    //
	// // Find all couples (a(i),b(j)) such that |a(i)|+|b(j)| = 3
	// a = -3:3
	// b = -2:2
	// [values,indices] = randnum_combineabssum(3,a,b)
	// S = sum(abs(values),"c")
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_combineabssum" , rhs , 1:1000 )
    apifun_checklhs ( "randnum_combineabssum" , lhs , 1:2 )
    //
    // Check type
    apifun_checktype ( "randnum_combineabssum" , s , "s" , 1 , "constant" )
    for i = 1 : length(varargin)
        ai = varargin(i)
        apifun_checktype ( "randnum_combineabssum" , ai , "a"+string(i) , i , "constant" )
    end
    //
    // Check size
    apifun_checkscalar ( "randnum_combineabssum" , s , "s" , 1 )
    for i = 1 : length(varargin)
        ai = varargin(i)
        apifun_checkvector ( "randnum_combineabssum" , ai , "a"+string(i) , i )
    end
    //
    // Check content
    apifun_checkflint ( "randnum_combineabssum" , s , "s" , 1 )
    apifun_checkgreq ( "randnum_combineabssum" , s , "s" , 1 , 0 )
    for i = 1 : length(varargin)
        ai = varargin(i)
        apifun_checkflint ( "randnum_combineabssum" , ai , "a"+string(i) , i )
    end

	//
	// Compute absolute values
    newargs = list()
    for i = 1 : length(varargin)
        newargs(i) = abs(varargin(i))
    end
    [absvalues,indices] = randnum_combinesum(s,newargs(:))
	//
	// Compute values (without the absolute value)
    values = zeros(absvalues)
    for i = 1 : length(varargin)
        xi = varargin(i);
        yi = xi(indices(:,i))
        values(:,i) = yi(:)
    end    
endfunction
