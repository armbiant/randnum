// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

k = 3;
s = 2;
d = 2;
values = randnum_combineabssumk(k,s,d);
expected = [
  -2.   0.  
  -1.  -1.  
  -1.   1.  
   0.  -2.  
   0.   2.  
   1.  -1.  
   1.   1.  
   2.   0.  
];
assert_checkequal(values,expected);
//
k = 5;
s = 2;
d = 3;
values = randnum_combineabssumk(k,s,d);
expected = [
  -2.   0.   0.  
  -1.  -1.   0.  
  -1.   0.  -1.  
  -1.   0.   1.  
  -1.   1.   0.  
   0.  -2.   0.  
   0.  -1.  -1.  
   0.  -1.   1.  
   0.   0.  -2.  
   0.   0.   2.  
   0.   1.  -1.  
   0.   1.   1.  
   0.   2.   0.  
   1.  -1.   0.  
   1.   0.  -1.  
   1.   0.   1.  
   1.   1.   0.  
   2.   0.   0.  
];
assert_checkequal(values,expected);
