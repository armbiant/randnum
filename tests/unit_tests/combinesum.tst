// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

function [values,indices] = mycombinesum(s,varargin)
// A fully vectorized implementation.
// Requires huge amounts of memory...

    //
    // Force the orientation
    largs = list()
    for i = 1 : length(varargin)
        ai = varargin(i)
        largs(i) = ai(:)'
    end
    // Compute the size of each arg.
    // e.g. n(1) = size(a,"*")
    n = []
    for i = 1 : length(varargin)
        n(i)= size(largs(i),"*");
    end
    // For each argument, make a vector of indices
    // e.g. combargs(1) = 1:n(1)
    combargs=list();
    for i = 1 : length(largs)
        combargs(i) = 1:n(i);
    end
    // Combine the vectors of indices.
    I = specfun_combine(combargs(:));
    // Compute the combinations of values.
    // e.g. Acombined(1,:) = a(I(1,:))
    Acombined = zeros(I);
    for i = 1 : length(largs)
        vi = largs(i);
        Acombined(i,:) = vi(I(i,:));
    end
    // Search for columns for which the sum is s.
    // We have SA(cols)==s.
    SA = sum(Acombined,"r");
    cols = find(SA==s);
    // Create the table of values, of uplets.
    values = [];
    indices = [];
    for i = 1 : length(largs)
        vi = largs(i);
        values(:,i) = Acombined(i,cols)';
        indices(:,i) = I(i,cols)';
    end
endfunction

values = randnum_combinesum(0,-3:3,-2:2);
expected = [
  -2.   2.  
  -1.   1.  
   0.   0.  
   1.  -1.  
   2.  -2.  
];
assert_checkequal(values,expected);
assert_checkequal(sum(values,"c"),zeros(5,1));
//
a = -3:3;
b = -2:2;
[values,indices] = randnum_combinesum(1,-3:3,-2:2);
expected = [
  -1.   2.  
   0.   1.  
   1.   0.  
   2.  -1.  
   3.  -2.  
];
assert_checkequal(values,expected);
expected  = [ 
    3.    5.  
    4.    4.  
    5.    3.  
    6.    2.  
    7.    1.  
];
assert_checkequal(indices,expected);
//
function checkcombinesum(s,a,b)
	a = a(:)
	b = b(:)
	[values,indices] = randnum_combinesum(s,a,b)
	m = size(values,"r")
	assert_checkequal(size(values),size(indices))
	assert_checkequal(a(indices(:,1)),values(:,1))
	assert_checkequal(b(indices(:,2)),values(:,2))
	assert_checkequal(sum(values,"c"),s*ones(m,1));
endfunction
for s = -10:3:10
	mprintf("Checking s=%d\n",s)
	checkcombinesum(s,-3:3,-2:2);
end
//
// Check with 4 arguments.
s = 0;
a = -3:3;
b = -2:2;
c = -1:1;
d = -2:2;
[values,indices] = randnum_combinesum(s,a,b,c,d);
m = size(values,"r");
assert_checkequal(a(indices(:,1)),values(:,1)');
assert_checkequal(b(indices(:,2)),values(:,2)');
assert_checkequal(c(indices(:,3)),values(:,3)');
assert_checkequal(d(indices(:,4)),values(:,4)');
assert_checkequal(sum(values,"c"),zeros(m,1));
//
//
a = 0:2;
for s = 0:5
	mprintf("Checking s=%d\n",s)
	[values1,indices1] = randnum_combinesum(s,a,a,a,a);
	[values2,indices2] = mycombinesum(s,a,a,a,a);
	assert_checkequal(values1,values2);
	assert_checkequal(indices1,indices2);
end
//
a = -3:3;
b = -2:2;
a = abs(a);
b = abs(b);
s = 2;
[values1,indices1] = randnum_combinesum(s,a,b);
[values2,indices2] = mycombinesum(s,a,b);
assert_checkequal(values1,values2);
assert_checkequal(indices1,indices2);
//
a = [-3 -2 2 2 2 1 2];
s = 2;
[values1,indices1] = randnum_combinesum(s,a);
[values2,indices2] = mycombinesum(s,a);
assert_checkequal(values1,values2);
assert_checkequal(indices1,indices2);
