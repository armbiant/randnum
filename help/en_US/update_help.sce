// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating randnum\n");
helpdir = cwd;
funmat = [
  "randnum_excel2002"
  "randnum_shrage"
  "randnum_dlaruv"
  "randnum_dlaruvstart"
  "randnum_parkmiller"
  "randnum_lcg"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "randnum";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the library help
mprintf("Updating randnum/lattice\n");
helpdir = fullfile(cwd,"lattice");
funmat = [
  "randnum_latticefind"
  "randnum_lcgplot"
  "randnum_plotlines"
  "randnum_planenumber"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "randnum";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the library help
mprintf("Updating randnum/support\n");
helpdir = fullfile(cwd,"support");
funmat = [
  "randnum_combinesum"
  "randnum_combineabssum"
  "randnum_combineabssumk"
  "randnum_combineabssumrep"
  "randnum_combinesumk"
  "randnum_autocorr"
  "randnum_lcgfullperiod"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "randnum";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
