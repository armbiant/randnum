// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demoCompositeModulus()
    // Notes on composite modulus

    // In "The Art of Computer Programming", Donald Knuth, Volume 2,
    // Seminumerical Algorithms, section 3.2.1.1 "Choice of modulus", 
    // we find the following comment. 
    // "Let w be the computer's word size, namely 2^e on a e-bit binary 
    // computer or 10^e on a e-digit decimal machine."
    // [...]
    // "The reader may well ask why we bother to consider using m=w+-1, when 
    // the choice m=w is so manifestly convenient. 
    // The reason is that when m=w, the right-hand digits of Xn are much 
    // less random than the left-hand digits."


    // In the following example, we study the right-hand digits 
    // of the multiplicative congruential generator with modulus m=2^5, 
    // multiplier a=5 and increment c=1. 
    // This generator has full period. 
    // We start by generating 32 values from this generator and check 
    // that all numbers in the range 0,1,...,31 have been generated.

    mprintf("\n\n")
    mprintf("Study of the LCG with parameters:\n")
    m = 2^5;
    a = 5;
    b = 1;
    mprintf("m=%d\n",m)
    mprintf("a=%d\n",a)
    mprintf("b=%d\n",b)
    n = m;
    seed = 0;
    [U,seed] = randnum_lcg(a,b,m,seed,n);
    mprintf("U*%d:\n",m)
    disp((U*m)')
    mprintf("Sorted in increasing order:\n")
    I = gsort(U'*m,"g","i");
    disp(I)
    mprintf("See the last bit (i.e. modulo 2).\n")
    mprintf("The last two bits have a period 2.\n")
    for k = 1 : m
        d = modulo(U(k)*m,2);
        mprintf("U(%d)*%d=%d (mod %d)=%d\n",..
        k,m,U(k)*m,2,d)
    end
    mprintf("See the last two bits (i.e. modulo 4).\n")
    mprintf("The last two bits have a period 2^2=4.\n")
    for k = 1 : m
        d = modulo(U(k)*m,4);
        mprintf("U(%d)*%d=%d (mod %d)=%d\n",..
        k,m,U(k)*m,4,d)
    end
    mprintf("See the last three bits (i.e. modulo 8).\n")
    mprintf("The last three bits have a period 2^3=8.\n")
    for k = 1 : m
        d = modulo(U(k)*m,8);
        mprintf("U(%d)*%d=%d (mod %d)=%d\n",..
        k,m,U(k)*m,8,d)
    end
    //
    mprintf("\n\n")
    mprintf("Study of the LCG with parameters:\n")
    m = 3*3*5
    a = number_lcm(3,5)+1
    b = 1
    mprintf("m=%d\n",m)
    mprintf("a=%d\n",a)
    mprintf("b=%d\n",b)
    seed = 0;
    [U,seed] = randnum_lcg(a,b,m,seed,m);
    mprintf("See the integers modulo 3.\n")
    mprintf("The cycle has length 3.\n")
    for k = 1 : m
        d = modulo(U(k)*m,3);
        mprintf("U(%d)*%d=%d (mod %d)=%d\n",..
        k,m,U(k)*m,3,d)
    end
    mprintf("See the integers modulo 5.\n")
    mprintf("The cycle has length 5.\n")
    for k = 1 : m
        d = modulo(U(k)*m,5);
        mprintf("U(%d)*%d=%d (mod %d)=%d\n",..
        k,m,U(k)*m,5,d)
    end
    //
    // Load this script into the editor
    //
    filename = "composite_modulus.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction 
demoCompositeModulus();
clear demoCompositeModulus

